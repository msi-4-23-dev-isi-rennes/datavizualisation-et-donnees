// Déclaration de la carte carte
const map = L.map('map').setView([47.218371, -1.553621], 18);

// Call API OpenStreetMap via leafletJS
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=sk.eyJ1IjoibXJ6aW1vMCIsImEiOiJjbDMwNHFqemwwMDdtM2xwaWZ1eDF3dXpoIn0.0NI07989fVmIRJlKMkTRIw', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'your.mapbox.access.token'
}).addTo(map);

// Adding icon for tram
const tramIcon = L.icon({
    iconUrl: 'Images/tram.png',
    iconSize: [20, 20]
});

// Adding icon for bus
const busIcon = L.icon({
    iconUrl: 'Images/bus.png',
    iconSize: [20, 20]
});

// Adding icon for busAndTram
const tramAndBusIcon = L.icon({
    iconUrl: 'Images/busTram.png',
    iconSize: [18, 26]
});


let arrayBus = [];
let arrayTram = [];
let arrayBusAndTram = [];
const xhttp = new XMLHttpRequest();

// Adding markers for bus and tram stops
xhttp.onreadystatechange = function () {
    if (this.readyState === 4 && this.status === 200) {
        let data = JSON.parse(this.responseText);
        let stopX;
        let stopY;
        let stopName;
        let stopId;
        let stopNbWheelchair;

        console.log(data);

        // Boucle, différenciation entre les bus et les tram et ajouts à leurs tableaux respectifs
        for (let i = 0; i < data.records.length; i++) {
            let point = data.records[i].geometry.coordinates;
            let fields = data.records[i].fields;
            let wheelchair_boarding = data.records[i].fields.wheelchair_boarding;

            if (wheelchair_boarding == undefined) {
                stopNbWheelchair = "0";
            } else {
                stopNbWheelchair = wheelchair_boarding;
            }

            if (fields.location_type == 1) {
                arrayBus.push(setStop(point[0], point[1], fields.stop_name, fields.stop_id, stopNbWheelchair));
            }

            if (fields.location_type == 0) {
                arrayTram.push(setStop(point[0], point[1], fields.stop_name, fields.stop_id, stopNbWheelchair));
            }
        }

        for (let i = 0; i < arrayBus.length; i++) {
            for (let j = 0; j < arrayTram.length; j++) {
                if (arrayBus[i].coordX === arrayTram[j].coordX && arrayBus[i].coordY === arrayTram[j].coordY) {
                    stopY = arrayBus[i].coordX;
                    stopX = arrayBus[i].coordY;
                    stopName = arrayBus[i].stopName;
                    stopId = arrayBus[i].stopId;
                    stopNbWheelchair = arrayBus[i].nbWheelchair;

                    arrayBusAndTram.push(setStop(stopX, stopY, stopName, stopId, stopNbWheelchair));

                    arrayTram.splice(j, 1)
                }
            }

            arrayBus.splice(i, 1);
        }

        // Ajout des icons des bus
        for (let i = 0; i < arrayBus.length; i++) {
            let busMarker = L.marker([arrayBus[i].coordY, arrayBus[i].coordX], {icon: busIcon});

            busMarker.on('mouseover', function () {
                busMarker.bindPopup(`Nom : ${arrayBus[i].stopName} </br> Fauteuils roulants : ${arrayBus[i].wheelChair}`).openPopup();
            });

            busMarker.addTo(map);
        }

        // Ajout des icons des tram
        for (let i = 0; i < arrayTram.length; i++) {
            let tramMarker = L.marker([arrayTram[i].coordY, arrayTram[i].coordX], {icon: tramIcon});

            tramMarker.on('mouseover', function () {
                tramMarker.bindPopup(`Nom : ${arrayTram[i].stopName} </br> Fauteuils roulants : ${arrayTram[i].wheelChair}`).openPopup();
            });

            tramMarker.addTo(map);
        }

        // Ajout des icons des busAndTram
        for (let i = 0; i < arrayBusAndTram.length; i++) {
            let tramAndBusMarker = L.marker([arrayBusAndTram[i].coordX, arrayBusAndTram[i].coordY], {icon: tramAndBusIcon});

            tramAndBusMarker.on('mouseover', function () {
                tramAndBusMarker.bindPopup(`Nom : ${arrayBusAndTram[i].stopName} </br> Fauteuils roulants : ${arrayBusAndTram[i].wheelChair}`).openPopup();
            });

            tramAndBusMarker.addTo(map);
        }

        console.log(arrayBus)
    }
}

xhttp.open("GET", "https://data.nantesmetropole.fr/api/records/1.0/search/?dataset=244400404_tan-arrets&q=&lang=fr&rows=-1");
xhttp.send();

// Adding bus and tram lines
xhttp.onreadystatechange = function () {
    if (this.readyState === 4 && this.status === 200) {

    }
}
xhttp.open("GET", "https://data.nantesmetropole.fr/api/records/1.0/search/?dataset=244400404_tan-arrets&q=&lang=fr&rows=-1");
xhttp.send();

// Fonction setStop
// Instancie un objet Stop et le retourne
// x => Coordonnée x de l'arrêt. Type : entier
// y => Coordonnée Y de l'arrêt. Type :entier
// name => Nom de l'arrêt. Type : string
// id => Id de l'arrêt. Type : string
// wheelchair => Nombre de fauteuils roulants pouvant être accueillant par le transport. Type : string
function setStop(x = 47.218371, y = -1.553621, name = "Nantes", id = "-1", wheelchair = "0") {
    return new Stop(x, y, name, id, wheelchair);
}
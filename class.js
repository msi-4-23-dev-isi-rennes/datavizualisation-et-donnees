class Stop{
    constructor(coordX, coordY, stopName, stopId, wheelChair)
    {
        this.coordX = coordX;
        this.coordY = coordY;
        this.stopName = stopName;
        this.stopId = stopId;
        this.wheelChair = wheelChair;
    }
}